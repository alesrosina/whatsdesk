const package = require("./package.json");
const fs = require("fs");
const del = require('del');
const copy = require('copy');
let update = {};

update.name = package.name;
update.version = package.version;
update.description = package.description;

fs.writeFileSync("./public/update.json",JSON.stringify(update),"utf-8");
del.sync("./public/whatsdesk_*.deb");
copy("./dist/whatsdesk_*.deb","./public/",()=>{});