import { app, BrowserWindow, ipcMain, Menu, Tray, globalShortcut } from 'electron';
import { enableLiveReload } from 'electron-compile';
import fs, { writeSync } from 'fs';
import path from 'path';
let notificationsActives = [];
let injectScripts = [
    "notifications.js",
    "links.js",
    "menucontextual.js"
];
//enableLiveReload();
let win = null;
let appIcon = null;
app.on("ready", _ => {
    globalShortcut.register('CommandOrControl+Q', () => {})
    appIcon = new Tray(path.join(__dirname, "icon", "logo.png"))
    win = new BrowserWindow({ show: false, icon: path.join(__dirname, "icon", "logo.png") });
    win.loadURL("https://web.whatsapp.com/");
    //win.webContents.openDevTools()
    win.on('ready-to-show', () => {
        win.show();
        for (let scriptName of injectScripts) {
            let script = fs.readFileSync(path.join(__dirname, "scripts", scriptName), "utf8");
            win.webContents.executeJavaScript(script, function () {
                //console.log(arguments);
            });
        }
    });
    win.on('page-title-updated', (evt, title) => {
        evt.preventDefault()
        title = title.replace(/(\([0-9]+\) )?.*/, "$1WhatsDesk");
        win.setTitle(title);
        appIcon.setToolTip(title);
        if (/\([0-9]+\)/.test(title)) {
            showNotification();
        } else {
            destroyNotification();
        }
    })
    win.on("focus", () => {
        destroyNotification();
        notificationsActives = [];
    });
    win.on('show', () => {
        appIcon.setHighlightMode('always')
    })
    win.on('hide', () => {
        appIcon.setHighlightMode('never')
    })
    win.on('close', function (event) {

        event.preventDefault();
        win.hide();

        //return false;
    });

    const contextMenu = Menu.buildFromTemplate([
        {
            label: 'Show/Hide WhatsDesk', click: function () {
                win.isVisible() ? win.hide() : win.show()
            }
        },
        {
            label: 'Quit', click: function () {
                win.destroy();
                app.quit();
            }
        }
    ])
    appIcon.on('click', () => {
        win.isVisible() ? win.hide() : win.show()
      })

    // Make a change to the context menu
    contextMenu.items[1].checked = false
    appIcon.setToolTip('WhatsDesk')

    // Call this again for Linux because we modified the context menu
    appIcon.setContextMenu(contextMenu)


})

function showNotification() {
    win.flashFrame(true);
    appIcon.setImage(path.join(__dirname, "icon", "unread.png"))
}
function destroyNotification() {
    win.flashFrame(false);
    appIcon.setImage(path.join(__dirname, "icon", "logo.png"))
}

ipcMain.on('notifications', (event, arg) => {
    if (!win.isFocused()) {
        notificationsActives.push(arg);
        showNotification();
    }
    event.sender.send('notification:new', true);
})
ipcMain.on('notification:close', (event, arg) => {
    let index = notificationsActives.indexOf(arg);
    if (index >= 0) {
        notificationsActives.splice(index, 1);
    }
    if (notificationsActives.length == 0) {
        destroyNotification();
    }
})
ipcMain.on('notification:click', (event, arg) => {
    win.show();
    win.focus();
    event.sender.send('notification:new', true);
})